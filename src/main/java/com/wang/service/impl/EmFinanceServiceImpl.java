package com.wang.service.impl;

import com.wang.model.entity.finance.EmFinance;
import com.wang.service.IEmFinanceService;
import org.springframework.stereotype.Service;

@Service
public class EmFinanceServiceImpl extends BaseServiceImpl<EmFinance> implements IEmFinanceService {
}
