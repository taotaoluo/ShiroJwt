package com.wang.model;

import com.wang.model.entity.finance.EmFinance;

import javax.persistence.Table;

@Table(name = "em_finance")
public class EmFinancesDto extends EmFinance {

    private Integer id;

    private String name;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
