package com.wang.mapper.finance;

import com.wang.model.entity.finance.EmFinance;
import tk.mybatis.mapper.common.Mapper;

public interface EmFinanceMapper extends Mapper<EmFinance> {
}