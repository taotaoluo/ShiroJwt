package com.wang.config.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class springScheduledDemo {
    @Scheduled(cron = "1/10 * * * * ?")
    public void testScheduled(){
        System.out.println("springScheduled run1:" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
    }


//    @Scheduled(cron = "1/3 * * * * ?")
//    public void testScheduled2(){
//        System.out.println("springScheduled run2:" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
//    }

}