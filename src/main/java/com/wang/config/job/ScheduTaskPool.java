package com.wang.config.job;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executors;

/**
 * 配置多线程的任务调度器
 * @author jiaqi.liu
 *
 */
@Configuration
public class ScheduTaskPool  implements SchedulingConfigurer {
    @Override
    public void configureTasks(ScheduledTaskRegistrar register) {

        //线程池中放置10个线程
        register.setScheduler(Executors.newScheduledThreadPool(10));
    }
}
