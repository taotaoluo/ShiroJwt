package com.wang.util.common;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * @author:luotao
 * @date:2019-11-19
 * @description:想写诗一样 不忘初心
 */
public class StandardTest {


    public static void main(String[] args) {
        StandardPBEStringEncryptor standardPBEStringEncryptor =new StandardPBEStringEncryptor();
        /*配置文件中配置如下的算法*/
        standardPBEStringEncryptor.setAlgorithm("PBEWithMD5AndDES");
        /*配置文件中配置的password*/
        standardPBEStringEncryptor.setPassword("EWRREWRERWECCCXC");
        /*要加密的文本*/
        String name = standardPBEStringEncryptor.encrypt("root");
        String password =standardPBEStringEncryptor.encrypt("WSQlight,.");
        /*将加密的文本写到配置文件中*/
        System.out.println("name="+name);
        System.out.println("password="+password);

    }


}
