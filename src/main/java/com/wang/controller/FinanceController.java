package com.wang.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wang.exception.CustomException;
import com.wang.model.EmFinancesDto;
import com.wang.model.common.BaseDto;
import com.wang.model.common.ResponseBean;
import com.wang.model.entity.finance.EmFinance;
import com.wang.service.IEmFinanceService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author:luotao
 * @date:2019-11-01
 * @description:想写诗一样 不忘初心
 */
@RestController
@RequestMapping("/finance")
public class FinanceController {


    private final IEmFinanceService finanService;


    @Autowired
    public FinanceController(IEmFinanceService finanService) {
        this.finanService = finanService;
    }

    @GetMapping
    @RequiresPermissions(logical = Logical.AND, value = {"finance:view"})
    public ResponseBean user(@Validated BaseDto baseDto) {
        if (baseDto.getPage() == null || baseDto.getRows() == null) {
            baseDto.setPage(1);
            baseDto.setRows(10);
        }
        PageHelper.startPage(baseDto.getPage(), baseDto.getRows());
        List<EmFinance> userDtos = finanService.selectAll();
        PageInfo<EmFinance> selectPage = new PageInfo<EmFinance>(userDtos);
        if (userDtos == null || userDtos.size() <= 0) {
            throw new CustomException("查询失败(Query Failure)");
        }
        Map<String, Object> result = new HashMap<String, Object>(16);
        result.put("count", selectPage.getTotal());
        result.put("data", selectPage.getList());
        return new ResponseBean( HttpStatus.OK.value(), "查询成功(Query was successful)", result);
    }




}
