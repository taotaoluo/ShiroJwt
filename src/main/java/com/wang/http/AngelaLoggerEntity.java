package com.wang.http;


import java.io.Serializable;

/**
 * User: wcy
 * Date: 2017/4/8
 * Time: 15:49
 */


public class AngelaLoggerEntity implements Serializable{

    private String id;
    //给子节点使用
    private String uuid;
    //接口名
    private String interfaceName;
    //接口ip地址
    private String interfaceIp;
    //接口ipport
    private Integer interfacePort;

    private String createTime;

    private String updateTime;
    //消耗时间
    private Long elapsedTime;
    //客户请求ip
    private String sourceIP;
    //所属租户信息
    private String tenentId;
    //请求参数
    private String parms;
    //请求结果
    private String result;
    //所属类型(error,debugger,info)
    private String type;
    //请求方式（INTERCEPTOR，HTTPCLIENT, LOGGER）
    private String pattern;
    //状态(8200,8500,200,500,404等)
    private String status;
    //警报等级（1：正常，2：警报，3：错误，4：严重错误）
    private Integer alarmLevel;
    //父节点的uuid，rpc或者httpclient请求时使用（拦截器时为空）
    private String parentNode;
    //HTTP请求消息中的请求方式
    private String method;
    //所属景区
    private String scenic;
    //所属项目
    private String project;
    //接口描述
    private String description;

    private String SourcrfaceIp;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getInterfaceIp() {
        return interfaceIp;
    }

    public void setInterfaceIp(String interfaceIp) {
        this.interfaceIp = interfaceIp;
    }

    public Integer getInterfacePort() {
        return interfacePort;
    }

    public void setInterfacePort(Integer interfacePort) {
        this.interfacePort = interfacePort;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(Long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public String getSourceIP() {
        return sourceIP;
    }

    public void setSourceIP(String sourceIP) {
        this.sourceIP = sourceIP;
    }

    public String getTenentId() {
        return tenentId;
    }

    public void setTenentId(String tenentId) {
        this.tenentId = tenentId;
    }

    public String getParms() {
        return parms;
    }

    public void setParms(String parms) {
        this.parms = parms;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(Integer alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    public String getParentNode() {
        return parentNode;
    }

    public void setParentNode(String parentNode) {
        this.parentNode = parentNode;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getScenic() {
        return scenic;
    }

    public void setScenic(String scenic) {
        this.scenic = scenic;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSourcrfaceIp() {
        return SourcrfaceIp;
    }

    public void setSourcrfaceIp(String sourcrfaceIp) {
        SourcrfaceIp = sourcrfaceIp;
    }
}
