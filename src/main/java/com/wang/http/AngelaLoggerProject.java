package com.wang.http;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

@Entity(name="logger_project")
public class AngelaLoggerProject implements Serializable {

    private Long id;
    //父节点（如果是根节点为0）
    private String parentNode;
    private String name;

    private String code;

    private Date createdDate;

//    private String updateTime;

    private String ip;

    private String path;

    private Date startTime;

    private Date shutDownTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParentNode() {
        return parentNode;
    }

    public void setParentNode(String parentNode) {
        this.parentNode = parentNode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getShutDownTime() {
        return shutDownTime;
    }

    public void setShutDownTime(Date shutDownTime) {
        this.shutDownTime = shutDownTime;
    }
}