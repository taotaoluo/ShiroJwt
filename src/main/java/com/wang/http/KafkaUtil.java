package com.wang.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * User: wcy
 * Date: 2017/11/6
 * Time: 11:22
 */
public class KafkaUtil {

    private KafkaUtil() {
    }

    private final static String LOGGER_TOPIC = "";

    private static ThreadPoolTaskExecutor poolTaskExecutor;

    private static PropertiesConfiguration config;

    public static KafkaProducer producer = null;

    private static Gson gson = new GsonBuilder().create();

    private static final Properties props = new Properties();

    static {
        try {
            poolTaskExecutor = new ThreadPoolTaskExecutor();
            //线程池所使用的缓冲队列
            poolTaskExecutor.setQueueCapacity(50000);
            //线程池维护线程的最少数量
            poolTaskExecutor.setCorePoolSize(5);
            //线程池维护线程的最大数量
            poolTaskExecutor.setMaxPoolSize(300);
            //线程池维护线程所允许的空闲时间
            poolTaskExecutor.setKeepAliveSeconds(30000);
            poolTaskExecutor.initialize();
            config = new PropertiesConfiguration("kafka.properties");
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        props.put("bootstrap.servers", config.getString("kafka.bootstrap-servers"));
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", config.getString("batch.size"));
        props.put("linger.ms", 1);
        props.put("buffer.memory", config.getString("buffer.memory"));
        props.put("key.serializer", config.getString("kafka.producer.key-serializer"));
        props.put("value.serializer", config.getString("kafka.producer.value-serializer"));
        checkKafka();
    }

    private static void sendMessage(AngelaLoggerEntity logger) {
        CompletableFuture.runAsync(() -> producer.send(new ProducerRecord("logger", gson.toJson(logger))), poolTaskExecutor);
    }

    /**
     * 发送消息到kafka,主题为test
     */
    public static void saveLogger(AngelaLoggerEntity logger) {
        CompletableFuture.runAsync(() ->
        {
            Future<RecordMetadata> f = producer.send(new ProducerRecord("logger", gson.toJson(logger)));
            try {
                System.out.println(f.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }, poolTaskExecutor);
    }

    public static void updateLogger(String id, AngelaLoggerEntity logger) {
        CompletableFuture.runAsync(() -> producer.send(new ProducerRecord("test1", gson.toJson(logger))), poolTaskExecutor);
    }

    public static void saveLoggerProject(AngelaLoggerProject logger) {
    }

    /**
     * 发送消息到kafka,定时向服务器发送服务运行正常的状态信息
     */
//    public static void checkProject() {
//        AngelaLoggerProjectNodeDetail detail = new AngelaLoggerProjectNodeDetail();
//        detail.setCode(LoggerThreadLocal.getProjectName());
//        try {
//            detail.setIp(InetAddressUtilities.getLocalHostLANAddress().getCanonicalHostName());
//            if (LoggerThreadLocal.getCurrentProject() != null)
//                detail.setPort(LoggerThreadLocal.getCurrentProject().getPort() + "");
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
//        CompletableFuture.runAsync(() -> producer.send(new ProducerRecord("checkproject", gson.toJson(detail))), poolTaskExecutor);
//    }

    /**
     * 发送消息到kafka,主题为test
     */
//    public static void startProject() {
//        AngelaLoggerProjectNode info = LoggerThreadLocal.getCurrentProject();
//        info.setCode(LoggerThreadLocal.getProjectName());
//        info.setScenic(LoggerThreadLocal.getScenicName());
//        try {
//            info.setIp(InetAddressUtilities.getLocalHostLANAddress().getHostAddress());
//            if (LoggerThreadLocal.getCurrentProject() != null)
//                info.setPort(LoggerThreadLocal.getCurrentProject().getPort() + "");
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
//        LoggerThreadLocal.setZchainProject(info);
////        CompletableFuture.runAsync(() -> producer.send(new ProducerRecord("startproject", gson.toJson(info))),poolTaskExecutor);
//    }

    /**
     * 发送消息，发送的对象必须是可序列化的
     */


    public static void main(String[] args) throws Exception {

        AngelaLoggerEntity entity = new AngelaLoggerEntity();
        entity.setDescription("aaaaaaaaaaaaa");
        entity.setMethod("/aaa/bbb/ccc");
        int i = 11;
        producer.send(new ProducerRecord("logger", gson.toJson(entity)));
        System.out.println("End");
        producer.close();
    }

    public static void checkKafka() {
        //消费者配置文件
        producer = new KafkaProducer(props);
    }
}
